InstallDir "C:\medkit"

# set the name of the installer
Outfile "installer.exe"

# Require admin
RequestExecutionLevel admin

Page directory

Section
  SetOutPath $INSTDIR
  ExecWait 'bin\mkd.bat uninstall & timeout 5'
SectionEnd

# Install files
Page instfiles

Section
  SetOutPath $INSTDIR
  File /r "releases"
  File /r "lib"
  File /r "erts-10.*"
  File /r "bin"
  File "vcredist_x64.exe"
SectionEnd

# Check and install the microsoft redistributable
!include x64.nsh

Section
  ReadRegStr $1 HKLM "SOFTWARE\Wow6432Node\Microsoft\VisualStudio\12.0\VC\Runtimes\x64" "Installed"
  StrCmp $1 1 installed

  # install required visual study dlls
  ExecWait 'vcredist_x64.exe'
  installed:
SectionEnd


Section
  SetOutPath $INSTDIR

  ExecWait 'bin\mkd.bat install && bin\mkd.bat start'
 # ExecWait 'bin\mkd.bat start'
  # ExecWait 'build\prod\rel\mkd\bin\mkd.bat start'
  # ExecWait 'build\prod\rel\mkd\erts-9.2\bin\erlsrv.exe start mkd_0.1.0'
SectionEnd

Section
  Quit
SectionEnd
