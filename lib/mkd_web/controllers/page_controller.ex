defmodule MkdWeb.PageController do
  use MkdWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
