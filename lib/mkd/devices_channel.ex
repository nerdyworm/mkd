defmodule Mkd.DevicesChannel do
  use PhoenixChannelClient

  def init(args) do
    IO.puts("INIT")
    IO.inspect(args)
    {:ok, args}
  end

  def handle_in("new_msg", payload, state) do
    {:noreply, state}
  end

  def handle_in("medkit.device.joined", payload, state) do
    %{
      "token" => _token,
      "topic" => topic
    } = payload

    IO.puts("JOINING PRIVATE CHANNEL")
    {:ok, channel} = Mkd.DeviceChannel.start_link(socket: state.socket, topic: topic)
    IO.puts("STARTED")
    IO.inspect(channel)

    results = PhoenixChannelClient.join(channel)
    IO.inspect(results)

    {:noreply, state}
  end

  def handle_reply({:ok, :join, resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:ok, "new_msg", resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:error, "new_msg", resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:timeout, "new_msg", _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:timeout, :join, _ref}, state) do
    {:noreply, state}
  end

  def handle_close(reason, state) do
    Process.send_after(self(), :rejoin, 5_000)
    {:noreply, state}
  end
end
