defmodule Mkd.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Mkd.Repo,
      {Mkd.Socket,
       [
         url: "ws://192.168.0.18:4000/devices/socket/websocket",
         serializer: Jason,
         params: %{token: ""},
         reconnect_interval: 5000
       ]},
      {Mkd.DevicesChannel, {[socket: Mkd.Socket, topic: "devices"], [name: Mkd.DevicesChannel]}},
      Mdk.Conn
      # {MkdWeb.Endpoint, []}

      # Starts a worker by calling: Mkd.Worker.start_link(arg)
      # {Mkd.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Mkd.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MkdWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
