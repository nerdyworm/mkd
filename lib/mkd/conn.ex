defmodule Mdk.Conn do
  use GenServer

  def start_link(_args) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(args) do
    send(self(), :init)
    {:ok, args}
  end

  def handle_info(:init, state) do
    results = PhoenixChannelClient.join(Mkd.DevicesChannel)
    IO.inspect(results)
    {:noreply, state}
  end
end
