defmodule Mkd.Repo do
  use Ecto.Repo,
    otp_app: :mkd,
    adapter: Ecto.Adapters.Postgres
end
