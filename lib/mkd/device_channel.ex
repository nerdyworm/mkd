defmodule Mkd.DeviceChannel do
  use PhoenixChannelClient
  require Logger

  def init(args) do
    {:ok, args}
  end

  def handle_in("ping", _payload, state) do
    Logger.info("ping")
    {:noreply, state}
  end

  def handle_in(name, payload, state) do
    IO.puts("<<< #{name} #{inspect(payload)}")
    {:noreply, state}
  end

  def handle_reply({:ok, :join, resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:ok, "new_msg", resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:error, "new_msg", resp, _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:timeout, "new_msg", _ref}, state) do
    {:noreply, state}
  end

  def handle_reply({:timeout, :join, _ref}, state) do
    {:noreply, state}
  end

  def handle_close(reason, state) do
    Process.send_after(self(), :rejoin, 5_000)
    {:noreply, state}
  end
end
